const defaultHeaders = {
  "content-type": "application/json",
  "sender": "webhook-redirect"
}

const consumers = [
    {
        name: "sendinblue-tracker",
        request: {
            url: 'https://webhook.site/51ac7b78-8854-44f9-ab7c-6ef6da6a4516',
            method: 'POST',
            headers: {
                "x-api-key": "test"
            }
        },
        triggers: {
          headers: [],
          body: [
            {
                type: 'regex',
                key: 'eventAction',
                value: '^.*(success)$'
            }
          ]
        }
    },
    {
        name: "google-ads",
        request: {
            url: 'https://webhook.site/4e459e21-829f-48dd-9326-556cf918b039',
            method: 'POST',
            headers: {}
        },
        triggers: {
          headers: [
              {
                type: 'regex',
                key: 'x-api-key',
                value: '^(.*)$'
            }
          ],
          body: []
        }
    }
];

exports.consumers = consumers;
exports.defaultHeaders = defaultHeaders;