This is a webhook redirect library.
It has been implemented at a client for mobile tracking.

![Architecture image](architecture.jpg)

# How does it work?
1. Any event emitted in the app is sent to our endpoint
2. The lib redirects and formats the hit to the appropriate consumers

# How to configure
1. Create an AWS lambda function with API endpoint trigger
2. Paste the code into the project
3. Add consumers in config.js
4. Add formatting rules for the body of each consumer in formatter.js
5. It works

Note: with a very few change it can work with any serverless cloud function provider like Cloudflare Workers, Google Cloud Functions, Vercel Cloud Functions...