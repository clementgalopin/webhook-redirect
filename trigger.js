const testTrigger = (trigger, entry) => {
	switch (trigger.type) {
		case 'regex':
			const regex = new RegExp(trigger.value);
			return regex.test(entry[trigger.key])
		case 'equal':
			return (trigger.value === entry[trigger.key])
		default:
			return false;
	}
}

const testEventVariableForConsumer = (event, eventVariable, consumer) => {
    if (Object.keys(event).indexOf(eventVariable) > -1 && event[eventVariable]) {
		for (let entryKey of Object.keys(event[eventVariable])) {
			let entry = {};
			entry[entryKey]	= event[eventVariable][entryKey];
			for (let trigger of consumer.triggers[eventVariable]) {
				if (trigger.key === entryKey && testTrigger(trigger, entry)) {
					return { status: true, logs: `${eventVariable} entry ${JSON.stringify(trigger.key)} has been matched by trigger.`};
					// return { status: true, logs: `${eventVariable} entry ${JSON.stringify(entry)} has been matched by trigger ${JSON.stringify(trigger)}`};
				}
			}
		}
	}
    return { status: false, logs: `Not trigger matched for ${eventVariable} entries`};
};

const shouldBeFired = (event, consumer) => {
    const testEventHeaders = testEventVariableForConsumer(event, 'headers', consumer);
    if (testEventHeaders.status === true) { 
        return testEventHeaders;
    }    
    const testEventBody = testEventVariableForConsumer(event, 'body', consumer);
    if (testEventBody.status === true) { 
        return testEventBody;
    }
	return { status: false, logs: `no entry has been matched for headers or body`};
};

exports.shouldBeFired = shouldBeFired;