const axios = require('axios');
const config = require('./config.js')

const sendRequest = async (consumer, event) => {
    return new Promise((resolve, reject) => {
        let output = {
            error: false, 
            status: {
                code: null, 
                text: null 
            },
            errorDetails: {
                no: null,
                code: null
            }
        }

        let requestOptions = { ...consumer.request };
        requestOptions.headers = Object.assign({}, requestOptions.headers, config.defaultHeaders, event.headers);
      
        const sendRequest = axios.request({ ...requestOptions, data: event.body })
        .then(function (response) {
            output.error = false;
            output.status.code = response.status;
            output.status.text = response.statusText;
            // console.log(output);
            return resolve(output);
        })
        .catch(function (error) {
            output.error = true;
            output.errorDetails.no = error.errno;
            output.errorDetails.code = error.code;
            // console.log(output);
            return resolve(output);
        });
    });
};

exports.sendRequest = sendRequest;