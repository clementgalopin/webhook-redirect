const trigger = require('./trigger.js');
const formatter = require('./formatter.js');
const sender = require('./sender.js');
const config = require('./config.js');

exports.handler = async (event, context) => {
    let body;
    let statusCode = '200';
    const headers = {
        'Content-Type': 'application/json',
    };
    let logs =  [];

    try {
        switch (event.httpMethod) {
            case 'POST':
                try {
                    event.body = JSON.parse(event.body)    
                
                } catch (e) {
                    event.body = {}
                    logs.push('Impossible to parse body');
                }
                
                logs.push('Received hit, starting to iterate over consumers');

                for (let consumer of config.consumers) {
                    logs.push(`Analysing triggers of consumer ${consumer.name}.`);
                
                    let triggerConsumer = trigger.shouldBeFired(event, consumer);
                    logs.push(triggerConsumer.logs);
                
                    if (triggerConsumer.status === true) {
                        logs.push(`consumer ${consumer.name} should be triggered.`);
                
                        let formattedEvent = formatter.formatEvent(event, consumer);
                
                        if (formattedEvent.error === true) {
                            logs.push(`An error happened while formatting the event: ${formattedEvent.errorDetails}.`);
                        } else {
                            logs.push(`event has been formatted to ${JSON.stringify(formattedEvent)}`);
                            logs.push(`Sending request`);
                
                            let request = await sender.sendRequest(consumer, formattedEvent.event);
                
                            if (request.error === true) {
                                logs.push(`Error in the request: ${JSON.stringify(request.errorDetails)}`);
                            } else {
                                logs.push(`Request sent with status: ${JSON.stringify(request.status)}`);
                            }
                        }
                    }
                }
                
                body = logs;
                
                break;
            default:
                throw new Error(`Unsupported method "${event.httpMethod}"`);
        }
    } catch (err) {
        statusCode = '400';
        body = err.message;
    } finally {
        body = JSON.stringify(body);
    }

    return {
        statusCode,
        body,
        headers,
    };
};