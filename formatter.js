var formatEvent = (event, consumer) => {
    let body = {}
    let headers = {};
    
    switch (consumer.name) {
      case 'sendinblue-tracker':
            if (!event.body.email) {
                return { event: null, error: true, errorDetails: 'No email has been provided.' };
            } else {
                body.email = event.body.email;
            }
            if (!event.body.eventCategory || !event.body.eventAction) {
                return { event: null, error: true, errorDetails: 'No eventCategory or eventAction has been provided.' };
            } else {
                body.event = [event.body.eventCategory, event.body.eventAction].join('|');
            }
      break;
      default:
        body = event.body;
    }
    
    return {
        event : {
            body,
            headers
        },
        error: false,
        errorDetails: null
    };
};

exports.formatEvent = formatEvent;